angular.module("videoStreamingApp")

.service("DataApiService", ["$http", function($http){

    var _dataApiService = this;

    _dataApiService.getVideos = function(dirUrl, callback) {

        $http.get("getVideos.php?dirUrl="+dirUrl).then(function(response){
            callback(response.data)
        }, function(error){
            callback(error);
        });
    }

}]);