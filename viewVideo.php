<?php
// $attachment_location = $_SERVER["DOCUMENT_ROOT"] . "/file.zip";
// $attachment_location = "/mnt/D850CB1E50CB026C/MOVIES/Downloading/War For The Planet Of The Apes (2017) [1080p] [YTS.AG]/War.For.The.Planet.Of.The.Apes.2017.1080p.BluRay.x264-[YTS.AG].mp4";
$attachment_location = $_GET['fileUrl'];

if (file_exists($attachment_location)) {

    header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
    header("Cache-Control: public"); // needed for internet explorer
    header("Content-Type: video/mp4");
    header("Content-Transfer-Encoding: Binary");
    header("Content-Length:" . filesize($attachment_location));
    // header("Content-Disposition: attachment; filename=file.zip");
    readfile($attachment_location);
    die();
} else {
    die("Error: File not found.");
}
