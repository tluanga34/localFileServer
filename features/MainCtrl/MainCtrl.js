angular.module("videoStreamingApp")

.controller("MainCtrl", ["DataApiService", function(DataApiService) {


    var _scope = this;

    DataApiService.getVideos(config.videoDir, function(response){
        console.log(response);
        _scope.videos = response;
    });


}]);